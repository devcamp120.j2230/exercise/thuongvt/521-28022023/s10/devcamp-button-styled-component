
import './App.css';
import { ChidButton, SuperButton } from './component/ExtentButton';
import {PropsButton,PrimaryButton} from './component/propsButton';
import StyleComponent from './component/styleComponent';

function App() {
  return (
    <div>
      {/* truyền trực tiếp */}
      <div>
        <StyleComponent>im a style button</StyleComponent>
      </div>
      {/* truyền kiểu props */}
      <div>
        <PropsButton bgColor="#FF31CA" fontColor="#000000">Button1</PropsButton>
        <PropsButton bgColor="#FF0000" fontColor="#000000">Button2</PropsButton>
        <PropsButton bgColor="#00DD00" fontColor="#000000">Button2</PropsButton>
        <PropsButton bgColor="" fontColor="">Button2</PropsButton>
      </div>
      {/*  */}
      <div>
      <PrimaryButton primary>Button1</PrimaryButton>
      <PrimaryButton>Button1</PrimaryButton>
      </div>
      {/*  */}
      <div>
      <SuperButton>Button1</SuperButton>
      <ChidButton>Hello 1 </ChidButton>
      <ChidButton primary>Hello 2 </ChidButton>
      </div>
      
    </div>
  );
}

export default App;

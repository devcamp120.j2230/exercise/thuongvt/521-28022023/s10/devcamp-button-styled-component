import styled from "styled-components";

const PropsButton = styled.button`
    background-color:${(props)=>props.bgColor||"red"};
    color:${(props)=>props.fontColor||"white"};
    font-size: 14px
    boder-radius:10px;
    padding:5px;
    margin:5px;

`
    // background-color:${(props)=>props.primary?"#FF31CA":"red"};
    // color:${(props)=>props.primary?"white":"#FF00FF"};
const PrimaryButton = styled.button `
    ${(props)=>props.primary?`background-color:#FF31CA;color:white `:`background-color:red;color:#FF00FF`};
    
    font-size: 14px
    boder-radius:10px;
    padding:5px;
    margin:5px;


`

export {PropsButton,PrimaryButton}
import styled from "styled-components";

export const SuperButton = styled.button`
    background-color:#363636;
    color:white;
    border:none;
    border-radius:50px;
    padding:10px;
    margin:10px;
    font-size: 14px
`

export const ChidButton = styled(SuperButton)`
background-color:${(props)=>props.primary?"#FF31CA":"red"};
color:${(props)=>props.primary?"white":"#FF00FF"};
`